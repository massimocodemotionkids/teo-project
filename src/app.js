//import { DraggableElement } from "./components/DraggableElement";
import { findDragging } from "./components/DragAndDropElement";

document.addEventListener('DOMContentLoaded', () => {

    const workspace = document.querySelector('main');
    workspace.addEventListener('dragover', (event) => {
        event.preventDefault();
    });
    workspace.addEventListener('drop', (event) => {
        event.preventDefault();

        const isInsideContainer = () => {
            const x = event.clientX;
            const y = event.clientY;
            for (let container of document.querySelectorAll('drop-container')) {
                const box = container.getBoundingClientRect();
                if (x > box.left && x < box.right && y > box.top && y < box.bottom) {
                    return true
                }
            }
            return false
        }

        if (!isInsideContainer()) {
            const draggedElement = findDragging();
            const newContainer = document.createElement('drop-container');

            newContainer.style.left = (event.clientX - draggedElement.offsetX - 16) + 'px';
            newContainer.style.top = (event.clientY - draggedElement.offsetY - 16) + 'px';

            newContainer.shadowRoot.appendChild(draggedElement);
            workspace.appendChild(newContainer)

        }
        //clean emply containers
        


    });


})
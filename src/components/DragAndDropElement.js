const HTML_TAG = 'draggable-element';
const HTML_TAG_CONTAINER = 'drop-container';

export const findDragging = () => {
    for (let container of Array.from(document.querySelectorAll('drop-container'))) {
        draggedElement = container.shadowRoot.querySelector(`.dragging`);
        if (draggedElement) {

            return draggedElement
        }
    }
}

export class DropContainer extends HTMLElement {
    constructor() {
        super();

        // Creazione dell'oggetto ShadowRoot e inserimento del contenuto
        const shadowRoot = this.attachShadow({ mode: 'open' });
        this.styleElement = document.createElement('style');
        this.styleElement.innerHTML = `
                /* Stile applicato al contenuto Shadow DOM */
                :host {
                    display: block;
                    position: absolute;
                    min-width: 100px;
                    padding: 0.5rem;
                    border: 2px dashed #fff;
                    background-color: transparent;
                    border-radius: 0.7rem;
                    font-family: sans-serif;
                }

                h1 {
                    color: hsl(${Math.floor(Math.random() * 360)}deg, 100%, 50%);
                }

        `;
        shadowRoot.appendChild(this.styleElement);
        
        this.addEventListener('dragover', this.handleDragOver);
        this.addEventListener('dragleave', this.handleDragLeave);
        this.addEventListener('drop', this.handleDrop);
    }
    handleDragLeave() {
        
    }
    connectedCallback() {
        const innerDraggables = Array.from(this.children);//Array.from(this.querySelectorAll('draggable-element'));
        for (let draggable of innerDraggables) {
            this.shadowRoot.appendChild(draggable)
        }
    }

    handleDragOver(event) {
        event.preventDefault();
        const getAfterElement = (container, y) => {
            const draggableElements = [...container.querySelectorAll('draggable-element:not(.dragging)')];
            return draggableElements.reduce((closest, child) => {
                const box = child.getBoundingClientRect();

                const offset = y - box.top - box.height / 2;
                if (offset < 0 && offset > closest.offset) {
                    return { offset: offset, element: child }
                } else {
                    return closest
                }

            }, { offset: Number.NEGATIVE_INFINITY }).element
        }


        // var draggedElement = document.querySelector('.dragging');
        // if (!draggedElement) {
        var draggedElement = findDragging();




        const afterElement = getAfterElement(this.shadowRoot, event.clientY);
        //console.log(afterElement)
        if (!afterElement) {
            this.shadowRoot.appendChild(draggedElement);
        } else {
            this.shadowRoot.insertBefore(draggedElement, afterElement);
        }


        // } else {
        //     const afterElement = getAfterElement(document, event.clientY);
        //     if (!afterElement) {
        //         this.shadowRoot.appendChild(draggedElement);
        //     } else {
        //         this.shadowRoot.insertBefore(afterElement, draggedElement);
        //     }
        // }

    }
    handleDrop(event) {
        event.preventDefault();
        
        //const blockID = event.dataTransfer.getData('text');




    }
}

export class DragAndDropElement extends HTMLElement {
    constructor() {
        super();

        // Creazione dell'oggetto ShadowRoot e inserimento del contenuto
        const shadowRoot = this.attachShadow({ mode: 'open' });
        const styleElement = document.createElement('style');
        styleElement.innerHTML = `
                /* Stile applicato al contenuto Shadow DOM */
                :host {
                    display: block;
                    
                    padding: 16px;
                    border: 1px solid #ccc;
                    background-color: #f8f8f8;
                }
        `;
        shadowRoot.appendChild(styleElement);
        this.content = document.createElement('div');
        shadowRoot.appendChild(this.content);
        // Abilita l'elemento per il trascinamento
        this.setAttribute('draggable', 'true');


        // this.addEventListener('dragenter', this.handleDragEnter);
        // this.addEventListener('dragover', this.handleDragOver);
        // this.addEventListener('dragleave', this.handleDragLeave);
        // this.addEventListener('drop', this.handleDrop);
    }

    // Gestore di evento dragstart
    handleDragStart(event) {
        event.currentTarget.classList.add("dragging");
        // Clear the drag data cache (for all formats/types)
        //event.dataTransfer.clearData();

        //event.dataTransfer.setData('text/plain', this.getAttribute('block-id'));

        // Calcola e memorizza l'offset rispetto al cursore del mouse
        this.offsetX = event.clientX - this.getBoundingClientRect().left;
        this.offsetY = event.clientY - this.getBoundingClientRect().top;
        
        //event.dataTransfer.setDragImage(event.target, this.offsetX, this.offsetY);

    }

    // Gestore di evento dragend
    handleDragEnd(event) {
        event.preventDefault();
        event.target.classList.remove('dragging');
        // Posiziona l'elemento trascinato nella posizione in cui è stato rilasciato
        // this.style.left = `${event.clientX - this.offsetX}px`;
        // this.style.top = `${event.clientY - this.offsetY}px`;

    }

    // Metodo chiamato quando il componente viene aggiunto al DOM
    connectedCallback() {
        //console.log('Il Web Component è stato aggiunto al DOM');
        this.content.innerHTML = this.innerHTML;

        // Aggiungi gestori di eventi per il drag and drop
        this.addEventListener('dragstart', this.handleDragStart);
        this.addEventListener('touchstart', this.handleDragStart);
        this.addEventListener('dragend', this.handleDragEnd);

        for (let container of document.querySelectorAll('drop-container')) {
            if (!container.shadowRoot)
                continue

            if (Array.from(container.shadowRoot.querySelectorAll('draggable-element')).length == 0 && Array.from(container.shadowRoot.children).length == 1)
                container.remove();
        }
    }

    // Metodo chiamato quando il componente viene rimosso dal DOM
    disconnectedCallback() {
        //console.log('Il Web Component è stato rimosso dal DOM');
        
        // Aggiungi gestori di eventi per il drag and drop
        this.removeEventListener('dragstart', this.handleDragStart);
        this.removeEventListener('dragend', this.handleDragEnd);
    }

    // Metodo chiamato quando uno degli attributi dell'elemento viene modificato
    attributeChangedCallback(name, oldValue, newValue) {
        console.log(`L'attributo ${name} è stato modificato da ${oldValue} a ${newValue}`);
    }

    // Metodo chiamato quando gli attributi osservati cambiano
    static get observedAttributes() {
        return ['custom-attribute'];
    }
}

// Registrazione del Custom Element
customElements.define(HTML_TAG, DragAndDropElement);
customElements.define(HTML_TAG_CONTAINER, DropContainer);